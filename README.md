# Enduro GPX Files
| Name                                                         | Distance | Moving Time | Image                                               |
| ------------------------------------------------------------ | -------- | ----------- | --------------------------------------------------- |
| [NSW - Tallaganda DSMRA Ride](https://gitlab.com/jteddy/enduro/raw/master/gpx/NSW%20-%20Tallaganda_DSMRA.gpx?inline=false) | 55.23km  | 3h          | ![Tallaganda](gpx/NSW - Tallaganda_DSMRA.png)       |
| [NSW - Dunns Creek Rd to Tomakin Single](https://gitlab.com/jteddy/enduro/raw/master/gpx/NSW%20-%20Dunns%20Creek%20Rd%20to%20Tomakin%20Single.gpx?inline=false) | 14.47km  | 36m         | ![Dunns Creed to Tomakin Single](gpx/NSW - Dunns Creek Rd to Tomakin Single.png) |
| [NSW - Runnyford Rd to Mogo](https://gitlab.com/jteddy/enduro/raw/master/gpx/NSW%20-%20Runnyford%20Rd%20to%20Mogo.gpx?inline=false) | 10km     | 28m         | ![Runnyford to Mogo](gpx/NSW - Runnyford Rd to Mogo.png)             |
| [Kowen Forest DSMRA - 2019 September](https://gitlab.com/jteddy/enduro/raw/master/gpx/Kowen%20Forest%20DSMRA%20-%202019%20September.gpx?inline=false) | 34km | 2h15m | ![Kowen](https://gitlab.com/jteddy/enduro/raw/master/gpx/Kowen%20Forest%20DSMRA%20-%202019%20September.png?inline=false) |

# Points of Interest

## Cotter

|  |
| ------------------------------- |
|**Name:** Loose Sandy Hill Challenge |
| **Map:** [Google](https://www.google.com/maps/dir//-35.4015258,148.9245475/@-35.4011596,148.8558999,12z) |
| **Latitude:** 35.401526º S |
| **Longitude:** 148.924548º E |

|  |
| ------------------------------- |
|**Name:** Nice hill however, is now over grown and a dead end at the top |
| **Map:** [Google](https://www.google.com/maps/dir//-35.3863396,148.9325336/@-35.3868539,148.8649613,12z) |
| **Latitude:** 35.386340º S |
| **Longitude:** 148.932534º E |

|  |
| ------------------------------- |
|**Name:** Trials Track |
| **Map:** [Google](https://www.google.com/maps/dir//-35.3781077,148.9343328/@-35.3774163,148.8634416,12z/data=!3m1!4b1) |
| **Latitude:** 35.378108º S |
| **Longitude:** 148.934333º E |

|  |
| ------------------------------- |
|**Name:** The secret car park that everyone knows |
| **Map:** [Google](https://www.google.com/maps/dir//-35.3400283,148.9423863/@-35.3418126,148.8752713,12z) |
| **Latitude:** 35.340028º S |
| **Longitude:** 148.942386º E |

# Events
| Event Name                                                   | Date & Time               | Location                                                     | Kms              | Cost  | Drive from CBR |
| ------------------------------------------------------------ | ------------------------- | ------------------------------------------------------------ | ---------------- | ----- | -------------- |
| [Sawmill Rally](https://www.admcc.com.au/sawmill-rally)      | 17 Nov, 6:30 am – 4:30 pm | Mansfield State Forest, 1380 Buttercup Rd, Merrijig VIC 3723, Australia | 2 loops          | $90   | 6h             |
| [Kenda Rally](https://www.admcc.com.au/kenda-rally)          | May 2020                  |                                                              | 1. 80km & 2 55km | $90   | 6h             |
| [Kowen Forest Ride 2020](https://www.facebook.com/kowenforestride/) | April 4/5 2020            | Kowen Forest                                                 |                  | $220  | 35m            |
| GRE                                                          | March 14/15               | Tenterfield, NSW                                             |                  | $185? | 11h            |
| GRE                                                          | April 18/19               | Corryong, Victoria                                           |                  |       | 4h             |
| GRE                                                          | June 27/28                | Poatina, Tasmania                                            |                  |       | 20h?           |
| GRE                                                          | July 18/19                | Goolgowie, NSW                                               |                  |       | 4h             |
| GRE                                                          | Sept 12/13                | Mount Perry, QLD                                             |                  |       | 16h            |
| GRE                                                          | November                  | TBA                                                          |                  |       |                |
| Wildwood Rock                                                | 8 Nov 2020                | 227 FEEHANS ROAD WILDWOOD VIC                                |                  |       |                |



- [Grassroots Enduro](https://grassrootsenduro.com.au/2020-race-series/) (GRE)
- [Kowen Forest Ride 2020](https://www.facebook.com/kowenforestride/)
- [ADMCC Calandar](https://www.admcc.com.au/public-calendar)

