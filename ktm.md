# KTM

## Fuel Pressure Gauge Tester

KTM OEM EFI Pressure Testing Tool - Part # 61029094000

Approx $350 AUD

![img](https://i.imgur.com/Gg5UpCA.png)

Tokyo Offroad has made a video on how to create you own DIY fuel pressure gauge tester. [KTM & Husky Fuel Pressure Measurement](https://www.youtube.com/watch?v=n9EVFgPZCKY)



Parts Required

- T-line: CPC Coupling (Black Plastic): 5850701050030 ([$103](https://www.ktmonlineparts.com.au/part/ktm/5850701050030#content))
  **T-Line**

  This part is a 5/16 Hose Barb Valved In-Line Coupling Insert

  Alternatives:

  - LCD22005?

  - CPC17005?
  - PLC17005?
  - [Motion Pro Motion Pro 5/16" Quick Disconnect Fuel-Line Coupling $42.95 AUD](https://www.mxstore.com.au/p/Motion-Pro-5-16-Quick-Disconnect-Fuel-Line-Couplin/08-120047)
  - [Ballards Quick Disconnect Black Fuel-Line Coupli $24.95 AUD](https://www.mxstore.com.au/p/Ballards-Quick-Disconnect-Black-Fuel-Line-Coupling/67-QUICK-K)

- Connector Fuel Pump (Metal): 77707010000  (Afermarket plastic connectors maybe cheaper) ([$27.40](https://www.ktmonlineparts.com.au/part/ktm/77707010000#content))

- O-ring CPC: 58507012000 ([$12](https://www.ktmonlineparts.com.au/part/ktm/58507012000#content))

- Fuel Line Hose (2 pieces 10cm?) (2 x [$20.55](https://www.ktmonlineparts.com.au/part/ktm/59007016000))

- T connector in the fuel pressure gauge set.

  - Aliexpress - [$42.80](https://www.aliexpress.com/item/32842964369.html?spm=a2g0o.productlist.0.0.6fb964f3UkbufY&algo_pvid=494ef62b-89c7-478b-bcc1-95c20dac95d0&algo_expid=494ef62b-89c7-478b-bcc1-95c20dac95d0-17&btsid=0ab6f82c15948904913332877e912f&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)

- Host Clamps - Oetiker Host Clamps with plyers (14.7mm needed)

  - Amazon - Snowinsping 80pc [$42.08](https://www.amazon.com.au/SNOWINSPRING-Stainless-Single-Clamps-Assortment/dp/B07YTY76QS/ref=sr_1_2?dchild=1&keywords=SNOWINSPRING+80Pcs+Stainless+Steel+Single&qid=1594890737&s=home-improvement&sr=1-2)
  - https://www.amazon.com.au/dp/B07XKV2SXG/ref=cm_sw_r_cp_api_i_1UE8EbBFQX31B
  - Host Clamp Plier - 60029057000

  

  A kit with a fuel pressure with exit valve is better

## Inline Fuel Filter
Earl's 731155ERL Vapor Guard Fuel Filter

 

From <https://www.amazon.com/gp/offer-listing/B07FXQYLY6/ref=dp_olp_unknown_mbc>

Another filter that they say the KTM team uses

https://www.amazon.com/Golan-Inc-Super-Filter-70-250G/dp/B00D3R1WOA/ref=pd_sim_263_1?_encoding=UTF8&pd_rd_i=B00D3R1WOA&pd_rd_r=GCX077SNARKB88HV7HYD&pd_rd_w=htv8n&pd_rd_wg=c4eig&psc=1&refRID=GCX077SNARKB88HV7HYD

https://thumpertalk.com/forums/topic/1227260-ktm-efi-in-line-fuel-filters/?tab=comments#comment-13706948

https://www.youtube.com/watch?v=n9EVFgPZCKY&feature=emb_logo



## Fuel Filter & Fuel Screen

The KTM in take fuel filter is a Mahle KL 97 (my one was made in Turkey)

KTM Fuel Screen 78141013190

Silencer Packing**

P121 Flass Fiber Yarn $175

KTM Part Number - 79005078640

https://www.motorcyclespareparts.eu/en/ktm-parts/79005078640 $88.50 + Shipping

 

https://au.fowlersparts.co.uk/parts/view/79005078640

https://www.amazon.com/gp/product/B07FXQYLY6/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1

https://ktmtalk.com/showthread.php?551132-KTM-EXC-F-Fuel-Filter-Mahle-97-vs-97-1&p=5409033#post5409033


## KTM Seat Height Lowering

| Name                     | Price AUD | Part #          | Site                                                         |
| ------------------------ | --------- | --------------- | ------------------------------------------------------------ |
| Seat Concepts Seat Cover | $395 + s  | 28-0159-30-1001 | https://www.ktm-parts.com/28-0159-30-1001.html               |
| Fisher Seat              |           |                 |                                                              |
| KTM Low Seat             | $228      | 79407940200     | [Peter Stevens](https://www.peterstevens.com.au/ktm-genuine-2017-low-seat-15mm-reduction.html) <br />[AOMC](https://www.ktm-parts.com/79407940200.html)<br />[$215 Blue City Motorcycles](https://bluecitymotorcycles.com.au/product/seat-exc-2017-18-models-79407940200) |
| Acerbis X-Seat           | $262 + s  |                 | https://www.motosport.com/acerbis-xseat                      |
|EE Complete Low Seat KTM 16-18|$186 + S||[AOMC](https://www.ktm-parts.com/75-616.html)|
|AliExpress Seat|$82||[$82](https://www.aliexpress.com/item/4000206507785.html?spm=a2g0o.detail.0.0.167725caVGjCsh&gps-id=pcDetailFavMayLike&scm=1007.12873.140318.0&scm_id=1007.12873.140318.0&scm-url=1007.12873.140318.0&pvid=e7791d36-f0b5-4c4d-b2ea-5841e825cdb9&_t=gps-id:pcDetailFavMayLike,scm-url:1007.12873.140318.0,pvid:e7791d36-f0b5-4c4d-b2ea-5841e825cdb9,tpp_buckets:668%230%23131923%2358_668%23808%235965%23399_668%23888%233325%2310_668%232846%238109%23248_668%232717%237560%23265_)|

Or just cut and plane the current seat.
